from setuptools import setup

setup(name='produtils',
      version='0.2',
      description='A nifty library of productivity tools',
      url='https://bitbucket.org/mukundraj/produtils',
      author='Mukund Raj',
      author_email='mukundraj2@gmail.com',
      license='MIT',
      packages=['produtils'],
      zip_safe=False)